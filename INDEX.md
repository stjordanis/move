# Move

Move files from one location to another


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## MOVE.LSM

<table>
<tr><td>title</td><td>Move</td></tr>
<tr><td>version</td><td>3.3b</td></tr>
<tr><td>entered&nbsp;date</td><td>2006-08-30</td></tr>
<tr><td>description</td><td>Move files from one location to another</td></tr>
<tr><td>keywords</td><td>freedos, move</td></tr>
<tr><td>author</td><td>Joe Cosentino &lt;hardmarine@comcast.net&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>Diego Rodriguez &lt;dieymir@yahoo.es&gt;</td></tr>
<tr><td>primary&nbsp;site</td><td>https://github.com/FDOS/move</td></tr>
<tr><td>alternate&nbsp;site</td><td>http://www.ibiblio.org/pub/micro/pc-stuff/freedos/files/dos/move/</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License, Version 2](LICENSE)</td></tr>
</table>
